# Дядя Максим представляет способы обновление образа и контейнера в Docker
## Как делаем ручками
Как работает. Мы сделали git clone нашего репозитория, после чего соответственно случилось обновление, прописываем git pull получаем обновленный репозиторий, далее docker build собираем новый образ, выключаем и удаляем действующий контейнер и docker run запускаем новый образ.
## Вариант 1 Самый простой, он есть и хорошо!

Для того, что бы данные действия оптимизировать и сделать процесс полу автоматическим пишем shell-скрипт, я напишу powershell (.ps1)
```
#Находим git, если shell, то cd
Set-Location "C:\Users\makcc\dokerlearn\dockerwelcome"
git pull
#Можем запустить заглушку уведомляющую, что в данный момент происходит обновление, но нужно ли, вопрос хороший.
#Собираем новый образ
docker build -t welcome-to-docker .
#Останавливаем контейнер
docker stop welcome-to-docker
#Удаляем контейнер
docker rm -f welcome-to-docker
#Запускаем новый контейнер
docker run -d -p 8088:3000 --name welcome-to-docker welcome-to-docker
```
Описание команд внутри скрипта, просто запускаем и радуемся  :>
## GitLab CI/CD Глава 2 - автоматизация :>

 На сервер устанавливается gitlab-runner. [Стать умнее(docs.gitlab)](https://docs.gitlab.com/ee/ci/quick_start/#create-a-gitlab-ciyml-file)
 Runner — это отдельное приложение, которое запускается для выполнения заданий CI/CD. [Настройка CI/CD в GitLab для синхронизации](https://www.dmosk.ru/miniinstruktions.php?mini=gitlab-runner-web)
 
 В репозитории добавляется файл .gitlab-ci.yml, в этом файле прописывается конвейер (какой то пошаговый скрипт) 
 ```
stages: #Конвейер, процесс выполнение будет отображаться в gitlab
  - build
  - deploy

build_image:
  stage: build
  before_script:
    - #Выполнение перед началом скрипта, можно прописать docker login
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - #Тут мы можем собрать образ, и запушить его в docker hub или не делать этого :>
    - docker build -t $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHORT_SHA .
    - docker push $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHORT_SHA
  only:
    - master

deploy_to_server:
  stage: deploy
  before_script:
    - #Выполнение перед началом скрипта, можно прописать docker login
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - #Выполняем pull загружаемся из docker hub или просто git pull :>
    - docker pull $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHORT_SHA
    -#Классика
    - docker stop my_container || true
    - docker rm my_container || true
    - docker run -d --name my_container -p 80:80 $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHORT_SHA
  only:
    - master
```
 В момент когда ты делаешь push у тебя сервак получает инфу, что у тебя удаленный репозиторий изменен и запускает на себе скрипт, ну и по сути делает все тоже самое, что я описал .ps1 скрипте

## Вариант 3 Использование git/hook

При git pull, будет будет выполнятся все тоже самое, что описано в shell скрипт, но просто shell скрипт, как то безопастнее.
```
В разработке. Хачу пицку :>
```

## Вариант 4 Использование конфига сборки docker

Какое то мега комбо если честно. В конфиге после определение директории git clone. Делаем провершел скриптик из 1 варианта, только без git pull, запускаем и радуемся.
> На самом деле не работает, просто я рукожоп, чуть позже разберусь

# Мини памятка

Maintainers should see [MAINTAINERS.md](MAINTAINERS.md).

Build and run:
```
docker build -t welcome-to-docker . 
docker run -d -p 8088:3000 --name welcome-to-docker welcome-to-docker
```
Open `http://localhost:8088` in your browser.
